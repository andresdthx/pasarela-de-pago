<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pay.home');
});

Route::get('/pay', 'PayController@getPing');
Route::get('/get/banks', 'PayController@getBanks');
Route::get('/validate/pay', 'PayController@getPing');
Route::post('/complete/payment/card', 'PayController@sendPaymentCard');
Route::post('/complete/payment/cash', 'PayController@sendPaymentCash');
Route::post('/complete/payment/tranfer', 'PayController@sendPaymentTransfer');
