<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PayController extends Controller
{
    private $ApiLogin;
    private $ApiKey;

    public function __construct(Request $request)
    {
        $this->ApiLogin = env('API_LOGIN');;
        $this->ApiKey   = env('API_KEY');;
        $this->baseUrl  = env('PAYU_API');
        $this->request  = $request;
        $this->getObject();
    }

    public function getPing(){
        try{
            $this->objRequest['command'] = 'PING';
            $response = $this->getConnection(json_encode($this->objRequest));
            $methods = $this->getPaymentMethods();
            $payment = $this->selectPaymentMethods($methods->paymentMethods);
            return view('pay.methods', compact('payment'));
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function selectPaymentMethods($paymentMethods){
        $payments = [];
        foreach ($paymentMethods as $payment) {
            if ($payment->country == "CO" && $payment->enabled) {
                array_push($payments, $payment);
            }
        }
        return $payments;
    }

    public function getPaymentMethods(){
        $this->objRequest['command'] = 'GET_PAYMENT_METHODS';
        return $this->getConnection(json_encode($this->objRequest));
    }

    public function getBanks(){
        $this->objRequest['command'] = 'GET_BANKS_LIST';
        $this->objRequest['bankListInformation'] = [
            'paymentMethod' => 'PSE',
            'paymentCountry' => 'CO'
        ];
        $response = $this->getConnection(json_encode($this->objRequest));
        return ($response->banks);
    }

    public function sendPaymentTransfer(){
        $this->objRequest['command'] = 'SUBMIT_TRANSACTION';
        $this->objRequest['bankListInformation'] = [
            'paymentMethod' => 'PSE',
            'paymentCountry' => 'CO'
        ];
        $this->objRequest['transaction'] = [
            "order"=> [
               "accountId"=> "512321",
               "referenceCode"=> "TestPayU",
               "description"=> "payment test",
               "language"=> "es",
               "signature"=> "7ee7cf808ce6a39b17481c54f2c57acc",
               "notifyUrl"=> "http://www.tes.com/confirmation",
               "additionalValues"=> [
                  "TX_VALUE"=> [
                     "value"=> 20000,
                     "currency"=> "COP"
               ],
                  "TX_TAX"=> [
                     "value"=> 3193,
                     "currency"=> "COP"
               ],
                  "TX_TAX_RETURN_BASE"=> [
                     "value"=> 16806,
                     "currency"=> "COP"
               ]
               ],
               "buyer"=> [
                  "emailAddress"=> "buyer_test@test.com"
               ]
            ],
            "payer"=> [
               "fullName"=> $this->request->name,
               "emailAddress"=> "payer_test@test.com",
               "contactPhone"=> $this->request->phone
            ],
            "extraParameters"=> [
               "RESPONSE_URL"=> "http://www.test.com/response",
               "PSE_REFERENCE1"=> $_SERVER['REMOTE_ADDR'],
               "FINANCIAL_INSTITUTION_CODE"=>$this->request->bank,
               "USER_TYPE"=> $this->request->type,
               "PSE_REFERENCE2"=> $this->request->identification,
               "PSE_REFERENCE3"=> $this->request->number
            ],
            "type"=> "AUTHORIZATION_AND_CAPTURE",
            "paymentMethod"=> "PSE",
            "paymentCountry"=> "CO",
            "ipAddress"=> $_SERVER['REMOTE_ADDR'],
            "cookie"=> "pt1t38347bs6jc9ruv2ecpv7o2",
            "userAgent"=> "Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
        ];
        $response = $this->getConnection(json_encode($this->objRequest));
        return response()->json($response);

    }

    public function sendPaymentCard(){
        $expirationDate = $this->request->year."/".$this->request->month;
        $this->objRequest['command'] = 'SUBMIT_TRANSACTION';
        $this->objRequest['transaction'] = [
            "order"=> [
               "accountId"=> "512321",
               "referenceCode"=> "TestPayU",
               "description"=> "payment test",
               "language"=> "es",
               "signature"=> "7ee7cf808ce6a39b17481c54f2c57acc",
               "notifyUrl"=> "http=>//www.tes.com/confirmation",
               "additionalValues"=> [
                  "TX_VALUE"=> [
                     "value"=> 20000,
                     "currency"=> "COP"
               ]
               ],
            ],
            "payer"=> [
               "fullName"=> $this->request->user,
               "billingAddress"=> [
                  "street1"=> $this->request->address
               ]
            ],
            "creditCard"=> [
               "number"=> $this->request->cardNumber,
               "securityCode"=> $this->request->code,
               "expirationDate"=> $expirationDate,
               "name"=> $this->request->method
            ],
            "extraParameters"=> [
               "INSTALLMENTS_NUMBER"=> $this->request->installments
            ],
            "type"=> "AUTHORIZATION_AND_CAPTURE",
            "paymentMethod"=> $this->request->method,
            "paymentCountry"=> "CO"
        ];

        $response = $this->getConnection(json_encode($this->objRequest));
        return response()->json($response);
    }

    public function sendPaymentCash(){
        $this->objRequest['command'] = 'SUBMIT_TRANSACTION';
        $this->objRequest['transaction'] = [
            "order" => [
                "accountId" => "512321",
                "referenceCode" => "TestPayU",
                "description" => "payment test",
                "language" => "es",
                "signature" => "7ee7cf808ce6a39b17481c54f2c57acc",
                "notifyUrl" => "http://www.test.com/confirmation",
                "additionalValues" => [
                   "TX_VALUE" => [
                      "value" => 20000,
                      "currency" => "COP"
                ],
                   "TX_TAX" => [
                      "value" => 3193,
                      "currency" => "COP"
                ],
                   "TX_TAX_RETURN_BASE" => [
                      "value" => 16806,
                      "currency" => "COP"
                ]
                ],
                "buyer" => [
                   "fullName" => "First name and last name",
                   "emailAddress" => "buyer_test@test.com"
                ]
             ],
             "type" => "AUTHORIZATION_AND_CAPTURE",
             "paymentMethod" => $this->request->method,
             "expirationDate" => "2020-10-18T00 =>00 =>00",
             "paymentCountry" => "CO",
             "ipAddress" => "127.0.0.1"
        ];
        $response = $this->getConnection(json_encode($this->objRequest));
        return response()->json($response);
    }

    public function getObject(){
        $this->objRequest = [
            "merchant" => [
                "apiLogin"=> $this->ApiLogin,
                "apiKey"=> $this->ApiKey
            ],
            'test' => true,
            'language'=> 'es',
            'command' => ''
        ];
    }

    public function getConnection($data){
        // try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curl, CURLOPT_URL, $this->baseUrl);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
               'Content-Type: application/json',
               'Accept: application/json'
            ));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            return json_decode(curl_exec($curl));

        //   } catch (\Exception $e) {
        //     return $e->getMessage();
        //   }
    }
}
