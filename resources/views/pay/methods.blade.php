@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Pagos
                </div>
                <div class="card-body">
                    <list-methods
                        :payments="{{json_encode($payment)}}"
                    ></list-methods>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
