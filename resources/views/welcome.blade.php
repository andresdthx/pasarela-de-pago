@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    PayU
                </div>
                <div class="card-body">

                    <form action="/validate/pay">
                        <button type="submit" class="btn btn-block btn-primary">Realizar pago</button>
                    </form>
                    {{-- <example-component></example-component> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
